﻿using UnityEngine;
public class TimeScale {

    static float maxTime = 60.0f;
    static float startTime;

    public static void Start() {
        startTime = Time.timeSinceLevelLoad;
    }

    public static float GetDifficultyScale() {
        float timePassed = Time.timeSinceLevelLoad - startTime;
        return Mathf.Clamp(timePassed / maxTime,0,1);
    }
}