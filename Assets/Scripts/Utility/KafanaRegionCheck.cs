﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D)), RequireComponent(typeof(SpriteRenderer))]
public class KafanaRegionCheck : MonoBehaviour {

    public Sprite activeSprite;
    Sprite inactiveSprite;

    Vector3 originalFlagPosition;
    BoxCollider2D playerCollider;
    BoxCollider2D selfColider;
    SpriteRenderer renderer;
    bool playerInside;

    void Start() {
        selfColider = GetComponent<BoxCollider2D>();
        renderer = GetComponent<SpriteRenderer>();
        inactiveSprite = renderer.sprite;
        playerInside = false;
    }

    void Update() {
        if(playerInside && Input.GetButton("Jump")) {
            GameController game = FindObjectOfType<GameController>();
            game.Play();
            TimeScale.Start();
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            playerInside = true;
            renderer.sprite = activeSprite;
        }
   }

    void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            playerInside = false;
            renderer.sprite = inactiveSprite;
        }
    }
}