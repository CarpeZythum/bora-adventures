﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController)), RequireComponent(typeof(AudioSource))]
public class Player : MonoBehaviour
{
	public float speed = 5.0f;
	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;

	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;

	float gravity;
	float jumpVelocity;
	float velocityXSmoothing;
	PlayerController controller;
	Vector2 velocity;
    AudioSource jumpSound;

	void Start() {
		controller = GetComponent<PlayerController> ();
		gravity = -(2 * jumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs (gravity) * timeToJumpApex;
        jumpSound = GetComponent<AudioSource>();
	}

	void Update() {

		if (controller.Collisions.above || controller.Collisions.below) {
			velocity.y = 0;
		}

		if (Input.GetButton ("Jump") && controller.Collisions.below) {
            velocity.y = jumpVelocity;
            jumpSound.Play();
            
		}

		velocity.y += gravity * Time.deltaTime;
		float targetVelocity = Input.GetAxisRaw("Horizontal") * speed;

        /*
         *  Running animation
         */
        if(Mathf.Abs(targetVelocity) > 0) {
            controller.SetRunning(true);
        } else {
            controller.SetRunning(false);
        }

		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocity, ref velocityXSmoothing, (controller.Collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);

		controller.Move (velocity * Time.deltaTime);
	}
}

