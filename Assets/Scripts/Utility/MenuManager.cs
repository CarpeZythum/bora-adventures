﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject playerPrefab;
    public Transform spawnLocation;

    public void OnStartClick() {
        mainMenu.SetActive(false);
        GameObject bird = GameObject.FindGameObjectWithTag("bird");
        if(bird != null) bird.SetActive(false);
        Instantiate(playerPrefab, spawnLocation.position, Quaternion.identity);
    }

    public void OnExitClick() {
        Application.Quit();
    }
}