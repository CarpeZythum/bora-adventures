﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
    public float minSpeed = 1.0f;
    public float maxSpeed = 5.0f;
    public GameObject platformPrefab;

    // Debug screen
    public GameObject debugScreen;

    GameObject lastPlatform;
    bool playing;
    Camera camera;

    void Start() {
        playing = false;
    }

    public void Play() {
        Instantiate(platformPrefab, new Vector3(-0.1f,  -1.5f, 0), Quaternion.identity);
        Instantiate(platformPrefab, new Vector3(-0.1f,  -0.5f, 0), Quaternion.identity);
        Instantiate(platformPrefab, new Vector3(-0.1f,  0.5f, 0), Quaternion.identity);
        Instantiate(platformPrefab, new Vector3(-0.1f,  1.5f, 0), Quaternion.identity);
        Instantiate(platformPrefab, new Vector3(-0.1f,  2.5f, 0), Quaternion.identity);
        playing = true;
        camera = Camera.main;
        lastPlatform = null;
    }

    public void End() {
        playing = false;
    }

    void Update() {
        float speed = Mathf.Lerp(minSpeed, maxSpeed, TimeScale.GetDifficultyScale());
        Debug.Log(speed);
        if(playing) {
            camera.transform.Translate(speed * Vector3.up * Time.deltaTime);
            if (lastPlatform == null) {
                SpawnBlock();            
            }
            else {
                float distance = lastPlatform.transform.position.y - camera.transform.position.y;
                if(distance <= 2.5f) {
                    SpawnBlock();
                }
            }
        }
    }
    
    void SpawnBlock() {
        Vector3 position = new Vector3(Random.Range(-1.0f, 1.0f), camera.transform.position.y + camera.orthographicSize + 0.5f, 0);
        lastPlatform = (GameObject)Instantiate(platformPrefab, position, Quaternion.identity);
    }
}