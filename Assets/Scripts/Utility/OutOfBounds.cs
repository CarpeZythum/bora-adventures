﻿using UnityEngine;
using System.Collections;

public class OutOfBounds : MonoBehaviour {
    Camera camera;

    void Start() {
        camera = Camera.main;
    }

    void Update() {
        if(transform.position.y < camera.transform.position.y - camera.orthographicSize) {
            Destroy(gameObject);
        }
    }
}