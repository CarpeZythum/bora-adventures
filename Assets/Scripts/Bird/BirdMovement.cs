﻿using UnityEngine;
using System.Collections;

public class BirdMovement : MonoBehaviour {

    public float speed = 1.0f;
    public Transform startPosition;
    float screenWidth;

    void Start() {
        screenWidth = Camera.main.aspect * Camera.main.orthographicSize;
    }

	void Update() {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
        if(transform.position.x > screenWidth) {
            transform.position = startPosition.position;
        }
    }
}
