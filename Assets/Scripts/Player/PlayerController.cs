﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D)), RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour {

    public int verticalRayCount = 4;
    public int horizontalRayCount = 4;
    public LayerMask layerMask;

    float skinWidth = 0.015f;
    float verticalRaySpacing;
    float horizontalRaySpacing;
    RayCastingLocations locations;
    BoxCollider2D collider;
    CollisionInfo collisions;
    Animator animatorController;

    public CollisionInfo Collisions
    {
        get
        {
            return collisions;
        }
    }

    void Start() {
        collider = GetComponent<BoxCollider2D>();
        animatorController = GetComponent<Animator>();
        CalculateRaySpacing();
    }

    void CalculateRayOrigins() {
        Bounds bounds = collider.bounds;
        bounds.Expand(skinWidth * -2);

        locations = new RayCastingLocations();
        locations.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        locations.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
        locations.topLeft = new Vector2(bounds.min.x, bounds.max.y);
        locations.topRight = new Vector2(bounds.max.x, bounds.max.y);
    }

    void CalculateRaySpacing() {
        Bounds bounds = collider.bounds;
        bounds.Expand(skinWidth * -2);

        verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);
        horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);

        verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
    }

    public void SetRunning(bool running) {
        animatorController.SetBool("running", running);
    }

    public void Move(Vector2 velocity) {
        CalculateRayOrigins();
        collisions.Reset();

        if (velocity.x != 0) {
            CheckForHorizontalCollisions(ref velocity);
        }
        if (velocity.y != 0) {
            CheckForVerticalCollisions(ref velocity);
        }

        transform.Translate(velocity);
    }

    void CheckForVerticalCollisions(ref Vector2 velocity) {
        float directionY = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y) + skinWidth;

        // remove the top collision
        if(directionY > 0) {
            return;
        }

        for (int i = 0; i < verticalRayCount; i++) {
            Vector2 rayOrigin = directionY == 1 ? locations.topLeft : locations.bottomLeft;
            rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, layerMask);

            if (hit) {
                velocity.y = (hit.distance - skinWidth) * directionY;
                rayLength = hit.distance;

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;
            }

            /*
             * Jumping animation
            */
            if (!collisions.below) {
                animatorController.SetBool("inAir", true);
            }

            if (collisions.below) {
                animatorController.SetBool("inAir", false);
            }

        }
    }

    void CheckForHorizontalCollisions(ref Vector2 velocity) {
        float directionX = Mathf.Sign(velocity.x);
        float rayLength = Mathf.Abs(velocity.x) + skinWidth;

        /*
		 *	Turning animation
		 **/
        if (directionX > 0) {
            animatorController.SetBool("turnedRight", true);
        } else if (directionX < 0) {
            animatorController.SetBool("turnedRight", false);
        }
        /* TODO: Remove unused collision code
        for (int i = 0; i < horizontalRayCount; i++) {
            Vector2 rayOrigin = (directionX == 1) ? locations.bottomRight : locations.bottomLeft;
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, layerMask);

            if (hit) {
                velocity.x = (hit.distance - skinWidth) * directionX;
                rayLength = hit.distance;

                collisions.left = directionX == -1;
                collisions.right = directionX == 1;
            }
        } */
    }

    struct RayCastingLocations {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }

    public struct CollisionInfo {
        public bool above, below;
        public bool left, right;

        public void Reset() {
            above = below = false;
            left = right = false;
        }
    }

}
