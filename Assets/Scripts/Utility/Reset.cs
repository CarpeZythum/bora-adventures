﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

public class Reset : MonoBehaviour {
    
	Camera camera;

	void Start() {
		camera = Camera.main;
	}

	void Update () {
#if UNITY_EDITOR
        if(Input.GetButton("Restart")) {
			EditorSceneManager.LoadScene (0);
		}
#endif
    }
}
